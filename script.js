

/*
Operators
	- Assignment
	- Arithmetic
	- Compound Assignment

	- Comparison
	- Logical

*/


/*Assignment Operator ( = )
	- used to assign a value to a variable

	example: 

	let a = 14; 
	console.log(a);

	a = 7;
	console.log(a);

	let b = a;
	console.log(a, b);
*/

/*
	Arithmetic Operator
	
	example:
*/

	// // Addition operator
	// 	console.log(20 + 10);

	// // Subtraction Operator
	// 	console.log(20 - 10);

	// // Multiplication operator
	// 	console.log(8 * 3)

	// // Division operator
	// 	console.log(25 / 5);

	// // Modulo operator (gets the remainder)
	// 	console.log(100 % 3);

	// /*Increment (++) Decrement (--)*/
	// let c = 30;

	// prefix
/*	console.log(++c); //31
	console.log(c); //31
	
	//suffix
	console.log(--c); //30
	console.log(c); //30

	console.log(c++); //30
	console.log(c--); //31*/

	/*Mini Activity*/

	// console.log(5 + 10);

	// let a = 5;
	// let b = 10;
	
	// console.log(a*b);

	// console.log((5*10)+8);


/*compound Assignment
	- perform arithmetic operation
	- assign back the value to the variable

	example:
*/
/*	let f = 15;

// Addition assignment operator (+=) - keeps the value
	console.log(f += 3); //18
	console.log(f += 3); //21
	console.log(f += 3); //24

//Subtraction assignment operator (-=)
	console.log(f -= 2); //22
	console.log(f -= 2); //20
	console.log(f -= 5); //15

//Multiplication assignment operator (*=)
	console.log(f *= 2); //30
	console.log(f *= 3); //90
	console.log(f *= 4); //360

//Division Assignment operator (/=)
	console.log(f /= 10);
	console.log(f /= 4);
	console.log(f /= 3);
______________________________

	f = 200
//Modulo assignment operator (%=)
	console.log(f %= 11); //2
	console.log(f %= 1); //0*/

/* Comparison Operators */
		/*Equality Operator (==) */
		// Inequality Operator (!=)
	 	//Strict Equality (===)
	 	//Strict Inequality 
	 //relation
	 	// - Greater than (>)
	 	// - less than (<)
	 	// - Greater than or equal to( >= )
	 	// - Less than or equal to ( <= )
	
//Equality Operator: 	
// - compares two operand and returns a logical value (boolean value)
	// console.log(typeof ("Jan" == "Jan") );
	// console.log("Jan" == "Jan")
	// console.log(true == true);
	// console.log(true == false);
	// console.log(false == true);
	// console.log(false == false);
	// console.log(null == undefined);
	// console.log(true == 1);
	// console.log(true == "1");

/*Inequality Operator (!=)
	- not equal operands
	-  evaluates whether operands are not equal
*/
/*	console.log("James" != "Daniel");
	console.log(3.00 != 3);
*/


/*Strict Equality (===)
	- compares "sameness of values" & data type
	- type coercion doesn't apply to this operator
*/

	// console.log(1===1);
	// console.log(2==="2");
	// console.log(true === 1);
	// console.log(null === undefined);

	// console.log(4 != 4);
	// console.log("4" != "four");
	// console.log(undefined != null);

/*
	Greater than ( > )
		console.log( 45 > 50);
		console.log( 50 > 49);
*/

/*
	Greater than or equal to ( >= )
		console.log(5.00 >= 5);
		console.log(70 >= 71);
*/

/*
	Les than (<=)
		console.log(45 < 50);
		console.log(50 < 49);

*/


/*
	Less than or equal to ( <= )
		console.log(5.00 <= 5);
		console.log(70 <= 71);

*/

/*
	Logical Operators
		- evaluates operands
		- returns boolean value
*/
	// AND oprator (&&)
		// checks whether all operands are in true value
	// console.log(true && true);
	// console.log(true && false);
	// console.log(false && true);
	// console.log(false && false);

/*	let g = true;
	let h = false;
	let i = 12;

	// first condition
	console.log( i < 2);
	// second condition
	console.log( g == h );
	// Third condition
	console.log( h && g);*/

/*
 OR operator ( || ) - double pipelines
 	//checks whether either of the operands is true
 	console.log( true || true )
 	console.log( true || false )
*/

/*
	NOT Operator (!)
		- negates the value (reverse)
*/
	/*console.log(!true);
	console.log(!false);*/

/*
	Selection Control Structure
		- if else statement
		- switch case
*/

/*	
		If statement

		syntax:
			if(condition){
				//statement
			}
	*/
/*
	let height = 180;

		if( height < 150 ) {
			console.log(`Below 150cm`)}
		 else {
			console.log(`Above 150cm`)
		}*/

/*function fName(Name){
	if(Name === `Romeo`){
		console.log(`Juliet`);
	} else{
		console.log(`Hamlet`);
	}
}

fName(`Romeo`)*/

	/*
		if...else if...else statement


	*/

/*	let price = 1000;

	if(price <= 20){
		console.log(`Affordable`);
	} else if(price <= 100){
		console.log(`Pricey but still affordable`);

	} else if(price <= 200){
		console.log(`Not affordable at the moment`);

	} else{
		console.log(`Not affordable at all.`);
	}*/

/*
function Typhoon(Windspeed){ 

	if(Windspeed <= 30){
		console.log(`not a typhoon`);
	} else if(Windspeed <= 61){
		console.log(`a tropical depression is detected`);
	} 
	else if(Windspeed >= 62 && Windspeed <= 88){
		console.log(`a tropical storm is detected`);
	} 
	else if(Windspeed >= 89 && Windspeed <= 117){
			console.log(`a severe tropical storm is detected`);
	} 
	else{
		console.log(`cannot idetify storm`);
	}
}

Typhoon(100);*/

	/*Ternary Operator
		- shorthand of if else statement

		syntax:

			(condition) ? <statement> : <statement>
	
	example:
	let myAge = 18;

	(myAge >= 18) ? console.log (`You are allowed`) : console.log(` You are not allowed here`); 

	*/

/*
	Switch Statement

	syntax:

		switch(condition){
			case <category>: <statement>
								break;
			default: <statement>
		}

	Example:
	let num = 8;

	switch(num){
		case 1 : console.log(`1`);
			break;
		case 2 : console.log(`2`);
			break;
		case 3 : console.log(`3`);
			break;
		case 4 : console.log(`4`);
			break;
		default: console.log(`not found`);
	}

*/

/*let user = prompt(`Enter your role`);

switch(user){
	case `admin` : console.log(`has authorized`);
		break;
	case `employee` : console.log(`ask for authorization`);
		break;
	default:  console.log(`no access`);
}
*/

// Interaction with HTML:
/*function color(){

	let text = document.getElementById(`input`).value
	let element = document.getElementById(`hello`)

switch(text){
	case `red`: element.style.color = `red`
		break;
	case `blue`: element.style.color = `blue`
		break; 
	case `green`: element.style.color = `green`
		break;
	default: element.style.color = `pink`
	}
}*/